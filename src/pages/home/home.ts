import { Component } from '@angular/core';
import { NavController,AlertController,reorderArray,ToastController } from 'ionic-angular';

import { ArchivedTodosPage } from '../archived-todos/archived-todos';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public todos = ['todo 1','todo 2','todo 3'];
  public reorderIsEnabled = false;
  public ArchivedTodosPage = ArchivedTodosPage;
  constructor(public navCtrl: NavController,private AlertController:AlertController,private ToastController:ToastController) {

  }
  remove(item){
    this.todos.splice(item,1);
  }

  edit(item){
    console.log(item);
    let newTodo = this.AlertController.create({
      title:'Add A Todo',
      message: 'Edit Your Todo',
      inputs: [
        {
          type: 'text',
          name: 'addTodoInput',
          value: this.todos[item]
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Save Todo',
          handler: (inputData)=>{
            this.todos[item] = inputData.addTodoInput;
            let AddToast = this.ToastController.create({
              message: 'Todo Saved!',
              duration: 2000
            });
            AddToast.present();
          }
        }
      ]
    });
    newTodo.present();
  }
  newTodo(){
    let newTodo = this.AlertController.create({
      title:'Add A Todo',
      message: 'Enter Your Todo',
      inputs: [
        {
          type: 'text',
          name: 'addTodoInput'
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Add Todo',
          handler: (inputData)=>{
            this.todos.push(inputData.addTodoInput);
            let AddToast = this.ToastController.create({
              message: 'Todo Added!',
              duration: 2000
            });
            AddToast.present();
          }
        }
      ]
    });
    newTodo.present();
  }
  ToggleReorder(){
     this.reorderIsEnabled = !this.reorderIsEnabled;
   }
   itemReorder($event){
     reorderArray(this.todos, $event);
   }

}
